<?php
defined( 'ABSPATH' ) || die();

//[dollar]
add_shortcode('crypto', 'cryptoHandle');
function cryptoHandle($params) {

            $cryptoToken = $params[0];
            $currency = $params[1];
            $html = Crypto::getRate($cryptoToken, $currency);
        
    return $html;
}
