<?php


class Crypto
{
    const API_KEY = "70c4fb74ca40922efc9b38ec";
    const BASE_URL = "https://min-api.cryptocompare.com/data/price?";

    public static function getRate(string $cryptoToken = null, string $currency = 'USD')
    {
        if (!empty($cryptoToken)) {
            $rate = self::getRequestCourse($cryptoToken, $currency);
            if (!$rate) {
                return '<div>неверно введены данные</div>';
            }
            return "<div class=\"dollar\">{$cryptoToken} / {$currency} = {$rate}</div>";
        }

        return "Введено пустое значение";
    }


    private static function getRequestCourse(string $cryptoToken, string $currency) 
    {
        $url = self::buildUrl("fsym={$cryptoToken}&tsyms={$currency}");
        $response = file_get_contents($url);


        if ($response) {
            try {
                $responseData = json_decode($response);

                if (isset ($responseData->Response)) {
                    return false;
                } 

                return $responseData->$currency;
            }
            catch (Exception $e) {
                var_dump($e);
            }
        }
        return $url;
    }

    private static function buildUrl(string $params) 
    {
        return self::BASE_URL. $params;
    }

}
