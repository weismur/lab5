<?php
/*
Plugin Name: Курсы крипто токенов
Description: Вывести курс криптотокена к доллару
Version: 1.0
Author: Weismur/Laxmsky
Author URI: https://t.me
*/

defined( 'ABSPATH' ) || die();
define('CRYPTO_DIR', dirname( __FILE__ ));

require_once CRYPTO_DIR . "/data.php";
require_once CRYPTO_DIR . "/shortcode.php";